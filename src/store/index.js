import { createStore } from 'vuex'
import request from '@/net/request'

export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
    getAgentInfo(ctx, params){
      const data = {
          url: '/api/agent-info',
          method: 'post',
          params: {
              ...params
          },
      }
      return request(data)
    },
  },
  modules: {
  }
})
